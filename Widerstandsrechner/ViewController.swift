//
//  ViewController.swift
//  Widerstandsrechner
//
//  Created by Michael Richter on 21.09.20.
//  Copyright © 2020 virtual hideout. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var picker1: UIPickerView!
    @IBOutlet weak var picker2: UIPickerView!
    @IBOutlet weak var picker3: UIPickerView!
    @IBOutlet weak var pickerMulti: UIPickerView!
    
    @IBOutlet weak var txtV: UITextField!
    @IBOutlet weak var txtR: UITextField!
    @IBOutlet weak var txtA: UITextField!
    @IBOutlet weak var switchVI: UISwitch!
    
    let colors = [
        UIColor.black,
        UIColor.brown,
        UIColor.red,
        UIColor.orange,
        UIColor.yellow,
        UIColor.green,
        UIColor.blue,
        UIColor.purple,
        UIColor.lightGray,
        UIColor.white,
        UIColor.clear
    ]
    
    let multiColors = [
        UIColor.darkGray,
        UIColor.orange,
        UIColor.black,
        UIColor.brown,
        UIColor.red,
        UIColor.orange,
        UIColor.yellow,
        UIColor.green,
        UIColor.blue,
        UIColor.purple,
        UIColor.lightGray,
        UIColor.white,
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.pickerMulti.selectRow(2, inComponent: 0, animated: false)
        self.picker1.selectRow(1, inComponent: 0, animated: false)
    }

    @IBAction func `switch`(_ sender: UISwitch) {
        self.txtA.isEnabled = sender.isOn
        self.txtV.isEnabled = !sender.isOn
    }
    
    
    func calc(){
        
        let a = "\(self.picker1.selectedRow(inComponent: 0))"
        let b = "\(self.picker2.selectedRow(inComponent: 0))"
        let c = self.picker3.selectedRow(inComponent: 0) < 10 ? "\(self.picker3.selectedRow(inComponent: 0))" : ""
        
        let m = self.pickerMulti.selectedRow(inComponent: 0)
        
        let power   = pow(10,m)
        let multi   = Double(truncating: power as NSNumber) / 100
        let R     = Double(a + b + c)! * multi
        
        if R >= 1000 {
            self.txtR.text = "\(R) Ohm / \(String(format:"%.1f", R/1000)) kOhm"
        } else {
            self.txtR.text = "\(R) Ohm"
        }
        
        /**
         Ohmsche Gesetz Formel/Gleichung:

         Formel:
         U = R · I
         I   = U/R
         R  = U/I
        
         "U" ist die Spannung in Volt, z.B. 1 V
         "R" ist der Widerstand in Ohm, z.B. 1 Ω
         "I" ist der Strom in Ampere, z.B. 1 A
         */
        
        if self.switchVI.isOn {
            let U = Double(self.txtV.text!)!
            let I = U / R
            self.txtA.text = "\(I)"
        } else {
            let I = Double(self.txtA.text!)!
            let U = I * R
            self.txtV.text = "\(U)"
        }
        
       
    }
}

extension Double {
    
}

extension ViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.picker1 || pickerView == self.picker2 {
            return self.colors.count - 1
        } else if pickerView == self.picker3 {
            return self.colors.count
        } else if pickerView == self.pickerMulti {
                   return self.multiColors.count
               }
        
        return 0
    }
    
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return row.zerofill(2)
//    }
//
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 50
    }
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 100
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let view = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 100))
        // view.font = UIFont(name: "Menlo", size: 32.0)
        view.textColor = UIColor.black
        view.text = ""
        view.textAlignment = .center
        if pickerView == self.pickerMulti {
            view.backgroundColor = self.multiColors[row]
        } else {
            view.backgroundColor = self.colors[row]
        }
        return view
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.calc()
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        self.calc()
        return true
    }
}
